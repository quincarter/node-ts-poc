import {ServerLoader, ServerSettings, GlobalAcceptMimesMiddleware} from "@tsed/common";
import "@tsed/typeorm";
import "@tsed/swagger";

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const rootDir = __dirname;

@ServerSettings({
    rootDir,
    mount: {
        "/api": `${rootDir}/controllers/**\/*.ts`
    },
    swagger: [
        {
            doc: "api-v1",
            path: "/v1/docs"
        }
    ],
    typeorm: [
        {
            name: 'postgres',
            type: 'postgres',
            port: '54320',
            username: "local",
            password: "secret",
            database: "local",
            entities: [
                `${__dirname}/db/entity/*{.ts,.js}`
            ],
            migrations: [
                `${__dirname}/db/migrations/*{.ts,.js}`
            ],
            subscribers: [
                `${__dirname}/db/subscriber/*{.ts,.js}`
            ]
        }
    ],
    acceptMimes: ["application/json"]
})
export class Server extends ServerLoader {
    /**
     * This method let you configure the express middleware required by your application to works.
     * @returns {Server}
     */
    public $onMountingMiddlewares(): void|Promise<any> {
        this
            .use(GlobalAcceptMimesMiddleware)
            .use(cookieParser())
            .use(compress({}))
            .use(methodOverride())
            .use(bodyParser.json())
            .use(bodyParser.urlencoded({
                extended: true
            }));

        return null;
    }
}
