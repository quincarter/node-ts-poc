import {
    Controller, Get, Render, Post,
    Authenticated, Required, BodyParams,
    Delete, PathParams, Patch, QueryParams
} from "@tsed/common";
import {Docs, Title} from "@tsed/swagger";
import {ActorService} from "../services/ActorService";
import {Actor} from "../db/entity/Actor";
import {DeleteResult} from "typeorm";

export interface Calendar{
    id: string;
    name: string;
}

@Controller("/actors")
@Docs("api-v1")
@Title("ActorController")
export class ActorCtrl {

    constructor(private actorService: ActorService) {
    }

    @Get("/")
    async find(): Promise<Array<Actor>> {
        return this.actorService.find();
    }

    @Get("/:id")
    async findActor(
        @Required() @PathParams("id") id: number,
    ): Promise<Actor> {
        return await this.actorService.findById(id);
    }

    @Post("/")
    @Authenticated()
    async post(
        @Required() @BodyParams("actor") actor: Actor
    ): Promise<Actor> {
        return await this.actorService.create(actor);
    }

    @Delete("/")
    @Authenticated()
    async deleteItem(
        @BodyParams("actor.actor_id") @Required() actor_id: number
    ): Promise<DeleteResult> {
        return this.actorService.delete(actor_id);
    }
}
