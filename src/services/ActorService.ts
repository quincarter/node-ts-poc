import {Service, AfterRoutesInit} from "@tsed/common";
import {TypeORMService} from "@tsed/typeorm";
import {Connection, DeleteResult} from "typeorm";
import {Actor} from "../db/entity/Actor";

@Service()
export class ActorService implements AfterRoutesInit {
    private connection: Connection;

    constructor(private typeORMService: TypeORMService) {
    }

    $afterRoutesInit() {
        this.connection = this.typeORMService.get("postgres");
    }

    async create(newActor: Actor): Promise<Actor> {
        return await new Promise<Actor>((resolve, reject) => {
            this.connection.getRepository(Actor).findOneOrFail({
                first_name: newActor.first_name,
                last_name: newActor.last_name
            })
                .then((data) => {
                    console.log("FULFILLED DATA", data);
                    if (data.actor_id){
                        reject("{\"error\": \"Actor Exists\"}")
                    }
                }).catch(() => {
                // no records found so save it
                resolve(this.save(newActor));
            });
        });
    }

    private async save(actor: Actor): Promise<Actor> {
        return await this.connection.getRepository(Actor).save(actor);
    }

    async find(): Promise<Array<Actor>> {
        return await new Promise<Array<Actor>>((resolve, reject) => {
            this.connection.manager.find(Actor)
                .then(data => {
                    if (data.length > 0) {
                        resolve(data);
                    }
                })
                .catch(() => {
                    reject("No Records Found");
                })
        });
    }

    async findById(id: number): Promise<Actor> {
        return await new Promise<Actor>((resolve, reject) => {
            this.connection.getRepository(Actor).findOne(id)
                .then(data => {
                    if (data.actor_id) {
                        resolve(data);
                    }
                }).catch(() => {
                    reject(`{"message": "No Records Found with ID: ${id}"}`);
            })
        });
    }
    
    async delete(actor_id: number): Promise<DeleteResult> {
        return await new Promise<DeleteResult>((resolve, reject) => {
            this.connection.getRepository(Actor).delete(actor_id)
                .then(data => {
                    if (data.affected > 0) {
                        data.raw = `Attempted to delete user with ID: ${actor_id}`;
                        resolve(data);
                    } else {
                        reject(`{"message": "No user exists with ID: ${actor_id}"}`);
                    }
                })
                .catch(() => {
                    reject(`{"message": "No user exists with ID: ${actor_id}"}`);
                })
        })
    }

}
