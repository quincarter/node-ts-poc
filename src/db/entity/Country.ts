import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {City} from "./City";


@Entity("country",{schema:"public" } )
export class Country {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"country_id"
        })
    country_id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:50,
        name:"country"
        })
    country:string;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        

   
    @OneToMany(type=>City, city=>city.country_)
    citys:City[];
    
}
