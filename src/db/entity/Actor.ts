import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {FilmActor} from "./FilmActor";
import {Property} from "@tsed/common";


@Entity("Actor",{schema:"public" } )
@Index("idx_actor_last_name",["last_name",])
export class Actor {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"actor_id"
        })
    @Property()
    actor_id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:45,
        name:"first_name"
        })
    @Property()
    first_name:string;
        

    @Column("character varying",{ 
        nullable:false,
        length:45,
        name:"last_name"
        })
    @Property()
    last_name:string;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        

   
    @OneToMany(type=>FilmActor, film_actor=>film_actor.actor_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    @Property()
    film_actors:FilmActor[];
    
}
