import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Film} from "./Film";


@Entity("Language",{schema:"public" } )
export class Language {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"language_id"
        })
    language_id:number;
        

    @Column("character",{ 
        nullable:false,
        length:20,
        name:"name"
        })
    name:string;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        

   
    @OneToMany(type=>Film, film=>film.language_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    films:Film[];
    
}
