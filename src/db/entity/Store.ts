import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Staff} from "./Staff";
import {Address} from "./Address";


@Entity("Store",{schema:"public" } )
@Index("idx_unq_manager_staff_id",["manager_staff_",],{unique:true})
export class Store {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"store_id"
        })
    store_id:number;
        

   
    @OneToOne(type=>Staff, staff=>staff.store,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'manager_staff_id'})
    manager_staff_:Staff | null;


   
    @ManyToOne(type=>Address, address=>address.stores,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'address_id'})
    address_:Address | null;


    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        
}
