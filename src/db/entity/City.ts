import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Country} from "./Country";
import {Address} from "./Address";


@Entity("city",{schema:"public" } )
@Index("idx_fk_country_id",["country_",])
export class City {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"city_id"
        })
    city_id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:50,
        name:"city"
        })
    city:string;
        

   
    @ManyToOne(type=>Country, country=>country.citys,{  nullable:false, })
    @JoinColumn({ name:'country_id'})
    country_:Country | null;


    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        

   
    @OneToMany(type=>Address, address=>address.city_)
    addresss:Address[];
    
}
