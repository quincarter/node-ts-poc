import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Customer} from "./Customer";
import {Staff} from "./Staff";
import {Rental} from "./Rental";


@Entity("Payment",{schema:"public" } )
@Index("idx_fk_customer_id",["customer_",])
@Index("idx_fk_rental_id",["rental_",])
@Index("idx_fk_staff_id",["staff_",])
export class Payment {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"payment_id"
        })
    payment_id:number;
        

   
    @ManyToOne(type=>Customer, customer=>customer.payments,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'customer_id'})
    customer_:Customer | null;


   
    @ManyToOne(type=>Staff, staff=>staff.payments,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'staff_id'})
    staff_:Staff | null;


   
    @ManyToOne(type=>Rental, rental=>rental.payments,{  nullable:false,onDelete: 'SET NULL',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rental_id'})
    rental_:Rental | null;


    @Column("numeric",{ 
        nullable:false,
        precision:5,
        scale:2,
        name:"amount"
        })
    amount:string;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        name:"payment_date"
        })
    payment_date:Date;
        
}
