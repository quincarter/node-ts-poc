import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {FilmCategory} from "./FilmCategory";


@Entity("Category",{schema:"public" } )
export class Category {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"category_id"
        })
    category_id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:25,
        name:"name"
        })
    name:string;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        

   
    @OneToMany(type=>FilmCategory, film_category=>film_category.category_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    film_categorys:FilmCategory[];
    
}
