import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Film} from "./Film";
import {Rental} from "./Rental";


@Entity("Inventory",{schema:"public" } )
@Index("idx_store_id_film_id",["film_","store_id",])
export class Inventory {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"inventory_id"
        })
    inventory_id:number;
        

   
    @ManyToOne(type=>Film, film=>film.inventorys,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'film_id'})
    film_:Film | null;


    @Column("smallint",{ 
        nullable:false,
        name:"store_id"
        })
    store_id:number;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        

   
    @OneToMany(type=>Rental, rental=>rental.inventory_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    rentals:Rental[];
    
}
