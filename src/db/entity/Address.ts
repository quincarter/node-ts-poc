import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {City} from "./City";
import {Customer} from "./Customer";
import {Staff} from "./Staff";
import {Store} from "./Store";


@Entity("address",{schema:"public" } )
@Index("idx_fk_city_id",["city_",])
export class Address {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"address_id"
        })
    address_id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:50,
        name:"address"
        })
    address:string;
        

    @Column("character varying",{ 
        nullable:true,
        length:50,
        name:"address2"
        })
    address2:string | null;
        

    @Column("character varying",{ 
        nullable:false,
        length:20,
        name:"district"
        })
    district:string;
        

   
    @ManyToOne(type=>City, city=>city.addresss,{  nullable:false, })
    @JoinColumn({ name:'city_id'})
    city_:City | null;


    @Column("character varying",{ 
        nullable:true,
        length:10,
        name:"postal_code"
        })
    postal_code:string | null;
        

    @Column("character varying",{ 
        nullable:false,
        length:20,
        name:"phone"
        })
    phone:string;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        

   
    @OneToMany(type=>Customer, customer=>customer.address_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    customers:Customer[];
    

   
    @OneToMany(type=>Staff, staff=>staff.address_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    staffs:Staff[];
    

   
    @OneToMany(type=>Store, store=>store.address_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    stores:Store[];
    
}
