import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Inventory} from "./Inventory";
import {Customer} from "./Customer";
import {Staff} from "./Staff";
import {Payment} from "./Payment";


@Entity("Rental",{schema:"public" } )
@Index("idx_unq_rental_rental_date_inventory_id_customer_id",["customer_","inventory_","rental_date",],{unique:true})
@Index("idx_fk_inventory_id",["inventory_",])
export class Rental {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"rental_id"
        })
    rental_id:number;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        name:"rental_date"
        })
    rental_date:Date;
        

   
    @ManyToOne(type=>Inventory, inventory=>inventory.rentals,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'inventory_id'})
    inventory_:Inventory | null;


   
    @ManyToOne(type=>Customer, customer=>customer.rentals,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'customer_id'})
    customer_:Customer | null;


    @Column("timestamp without time zone",{ 
        nullable:true,
        name:"return_date"
        })
    return_date:Date | null;
        

   
    @ManyToOne(type=>Staff, staff=>staff.rentals,{  nullable:false, })
    @JoinColumn({ name:'staff_id'})
    staff_:Staff | null;


    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        

   
    @OneToMany(type=>Payment, payment=>payment.rental_,{ onDelete: 'SET NULL' ,onUpdate: 'CASCADE' })
    payments:Payment[];
    
}
