import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Language} from "./Language";
import {FilmActor} from "./FilmActor";
import {FilmCategory} from "./FilmCategory";
import {Inventory} from "./Inventory";


@Entity("Film",{schema:"public" } )
@Index("film_fulltext_idx",["fulltext",])
@Index("idx_fk_language_id",["language_",])
@Index("idx_title",["title",])
export class Film {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"film_id"
        })
    film_id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:255,
        name:"title"
        })
    title:string;
        

    @Column("text",{ 
        nullable:true,
        name:"description"
        })
    description:string | null;
        

    @Column("integer",{ 
        nullable:true,
        name:"release_year"
        })
    release_year:number | null;
        

   
    @ManyToOne(type=>Language, language=>language.films,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'language_id'})
    language_:Language | null;


    @Column("smallint",{ 
        nullable:false,
        default: () => "3",
        name:"rental_duration"
        })
    rental_duration:number;
        

    @Column("numeric",{ 
        nullable:false,
        default: () => "4.99",
        precision:4,
        scale:2,
        name:"rental_rate"
        })
    rental_rate:string;
        

    @Column("smallint",{ 
        nullable:true,
        name:"length"
        })
    length:number | null;
        

    @Column("numeric",{ 
        nullable:false,
        default: () => "19.99",
        precision:5,
        scale:2,
        name:"replacement_cost"
        })
    replacement_cost:string;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        

    @Column("text",{ 
        nullable:true,
        array:true,
        name:"special_features"
        })
    special_features:string[] | null;
        

    @Column("tsvector",{ 
        nullable:false,
        name:"fulltext"
        })
    fulltext:string;
        

   
    @OneToMany(type=>FilmActor, film_actor=>film_actor.film_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    film_actors:FilmActor[];
    

   
    @OneToMany(type=>FilmCategory, film_category=>film_category.film_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    film_categorys:FilmCategory[];
    

   
    @OneToMany(type=>Inventory, inventory=>inventory.film_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    inventorys:Inventory[];
    
}
