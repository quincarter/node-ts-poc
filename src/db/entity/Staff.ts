import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Address} from "./Address";
import {Payment} from "./Payment";
import {Rental} from "./Rental";
import {Store} from "./Store";


@Entity("Staff",{schema:"public" } )
export class Staff {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"staff_id"
        })
    staff_id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:45,
        name:"first_name"
        })
    first_name:string;
        

    @Column("character varying",{ 
        nullable:false,
        length:45,
        name:"last_name"
        })
    last_name:string;
        

   
    @ManyToOne(type=>Address, address=>address.staffs,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'address_id'})
    address_:Address | null;


    @Column("character varying",{ 
        nullable:true,
        length:50,
        name:"email"
        })
    email:string | null;
        

    @Column("smallint",{ 
        nullable:false,
        name:"store_id"
        })
    store_id:number;
        

    @Column("boolean",{ 
        nullable:false,
        default: () => "true",
        name:"active"
        })
    active:boolean;
        

    @Column("character varying",{ 
        nullable:false,
        length:16,
        name:"username"
        })
    username:string;
        

    @Column("character varying",{ 
        nullable:true,
        length:40,
        name:"password"
        })
    password:string | null;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        

    @Column("bytea",{ 
        nullable:true,
        name:"picture"
        })
    picture:Buffer | null;
        

   
    @OneToMany(type=>Payment, payment=>payment.staff_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    payments:Payment[];
    

   
    @OneToMany(type=>Rental, rental=>rental.staff_)
    rentals:Rental[];
    

   
    @OneToOne(type=>Store, store=>store.manager_staff_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    store:Store | null;

}
