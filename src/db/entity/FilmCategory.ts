import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Film} from "./Film";
import {Category} from "./Category";


@Entity("FilmCategory",{schema:"public" } )
export class FilmCategory {

   
    @ManyToOne(type=>Film, film=>film.film_categorys,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'film_id'})
    film_:Film | null;


   
    @ManyToOne(type=>Category, category=>category.film_categorys,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'category_id'})
    category_:Category | null;


    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        
}
