import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Actor} from "./Actor";
import {Film} from "./Film";


@Entity("FilmActor",{schema:"public" } )
@Index("idx_fk_film_id",["film_",])
export class FilmActor {

   
    @ManyToOne(type=>Actor, actor=>actor.film_actors,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'actor_id'})
    actor_:Actor | null;


   
    @ManyToOne(type=>Film, film=>film.film_actors,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'film_id'})
    film_:Film | null;


    @Column("timestamp without time zone",{ 
        nullable:false,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date;
        
}
