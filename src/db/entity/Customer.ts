import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Address} from "./Address";
import {Payment} from "./Payment";
import {Rental} from "./Rental";


@Entity("Customer",{schema:"public" } )
@Index("idx_fk_address_id",["address_",])
@Index("idx_last_name",["last_name",])
@Index("idx_fk_store_id",["store_id",])
export class Customer {

    @PrimaryGeneratedColumn({
        type:"integer", 
        name:"customer_id"
        })
    customer_id:number;
        

    @Column("smallint",{ 
        nullable:false,
        name:"store_id"
        })
    store_id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:45,
        name:"first_name"
        })
    first_name:string;
        

    @Column("character varying",{ 
        nullable:false,
        length:45,
        name:"last_name"
        })
    last_name:string;
        

    @Column("character varying",{ 
        nullable:true,
        length:50,
        name:"email"
        })
    email:string | null;
        

   
    @ManyToOne(type=>Address, address=>address.customers,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'address_id'})
    address_:Address | null;


    @Column("boolean",{ 
        nullable:false,
        default: () => "true",
        name:"activebool"
        })
    activebool:boolean;
        

    @Column("date",{ 
        nullable:false,
        default: () => "('now')::date",
        name:"create_date"
        })
    create_date:string;
        

    @Column("timestamp without time zone",{ 
        nullable:true,
        default: () => "now()",
        name:"last_update"
        })
    last_update:Date | null;
        

    @Column("integer",{ 
        nullable:true,
        name:"active"
        })
    active:number | null;
        

   
    @OneToMany(type=>Payment, payment=>payment.customer_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    payments:Payment[];
    

   
    @OneToMany(type=>Rental, rental=>rental.customer_,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    rentals:Rental[];
    
}
