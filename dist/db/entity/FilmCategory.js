"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Film_1 = require("./Film");
const Category_1 = require("./Category");
let FilmCategory = class FilmCategory {
};
__decorate([
    typeorm_1.ManyToOne(type => Film_1.Film, film => film.film_categorys, { primary: true, nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'film_id' }),
    __metadata("design:type", Film_1.Film)
], FilmCategory.prototype, "film_", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Category_1.Category, category => category.film_categorys, { primary: true, nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'category_id' }),
    __metadata("design:type", Category_1.Category)
], FilmCategory.prototype, "category_", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], FilmCategory.prototype, "last_update", void 0);
FilmCategory = __decorate([
    typeorm_1.Entity("FilmCategory", { schema: "public" })
], FilmCategory);
exports.FilmCategory = FilmCategory;
//# sourceMappingURL=FilmCategory.js.map