"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Film_1 = require("./Film");
const Rental_1 = require("./Rental");
let Inventory = class Inventory {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "inventory_id"
    }),
    __metadata("design:type", Number)
], Inventory.prototype, "inventory_id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Film_1.Film, film => film.inventorys, { nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'film_id' }),
    __metadata("design:type", Film_1.Film)
], Inventory.prototype, "film_", void 0);
__decorate([
    typeorm_1.Column("smallint", {
        nullable: false,
        name: "store_id"
    }),
    __metadata("design:type", Number)
], Inventory.prototype, "store_id", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], Inventory.prototype, "last_update", void 0);
__decorate([
    typeorm_1.OneToMany(type => Rental_1.Rental, rental => rental.inventory_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Inventory.prototype, "rentals", void 0);
Inventory = __decorate([
    typeorm_1.Entity("Inventory", { schema: "public" }),
    typeorm_1.Index("idx_store_id_film_id", ["film_", "store_id",])
], Inventory);
exports.Inventory = Inventory;
//# sourceMappingURL=Inventory.js.map