"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Language_1 = require("./Language");
const FilmActor_1 = require("./FilmActor");
const FilmCategory_1 = require("./FilmCategory");
const Inventory_1 = require("./Inventory");
let Film = class Film {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "film_id"
    }),
    __metadata("design:type", Number)
], Film.prototype, "film_id", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 255,
        name: "title"
    }),
    __metadata("design:type", String)
], Film.prototype, "title", void 0);
__decorate([
    typeorm_1.Column("text", {
        nullable: true,
        name: "description"
    }),
    __metadata("design:type", String)
], Film.prototype, "description", void 0);
__decorate([
    typeorm_1.Column("integer", {
        nullable: true,
        name: "release_year"
    }),
    __metadata("design:type", Number)
], Film.prototype, "release_year", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Language_1.Language, language => language.films, { nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'language_id' }),
    __metadata("design:type", Language_1.Language)
], Film.prototype, "language_", void 0);
__decorate([
    typeorm_1.Column("smallint", {
        nullable: false,
        default: () => "3",
        name: "rental_duration"
    }),
    __metadata("design:type", Number)
], Film.prototype, "rental_duration", void 0);
__decorate([
    typeorm_1.Column("numeric", {
        nullable: false,
        default: () => "4.99",
        precision: 4,
        scale: 2,
        name: "rental_rate"
    }),
    __metadata("design:type", String)
], Film.prototype, "rental_rate", void 0);
__decorate([
    typeorm_1.Column("smallint", {
        nullable: true,
        name: "length"
    }),
    __metadata("design:type", Number)
], Film.prototype, "length", void 0);
__decorate([
    typeorm_1.Column("numeric", {
        nullable: false,
        default: () => "19.99",
        precision: 5,
        scale: 2,
        name: "replacement_cost"
    }),
    __metadata("design:type", String)
], Film.prototype, "replacement_cost", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], Film.prototype, "last_update", void 0);
__decorate([
    typeorm_1.Column("text", {
        nullable: true,
        array: true,
        name: "special_features"
    }),
    __metadata("design:type", Array)
], Film.prototype, "special_features", void 0);
__decorate([
    typeorm_1.Column("tsvector", {
        nullable: false,
        name: "fulltext"
    }),
    __metadata("design:type", String)
], Film.prototype, "fulltext", void 0);
__decorate([
    typeorm_1.OneToMany(type => FilmActor_1.FilmActor, film_actor => film_actor.film_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Film.prototype, "film_actors", void 0);
__decorate([
    typeorm_1.OneToMany(type => FilmCategory_1.FilmCategory, film_category => film_category.film_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Film.prototype, "film_categorys", void 0);
__decorate([
    typeorm_1.OneToMany(type => Inventory_1.Inventory, inventory => inventory.film_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Film.prototype, "inventorys", void 0);
Film = __decorate([
    typeorm_1.Entity("Film", { schema: "public" }),
    typeorm_1.Index("film_fulltext_idx", ["fulltext",]),
    typeorm_1.Index("idx_fk_language_id", ["language_",]),
    typeorm_1.Index("idx_title", ["title",])
], Film);
exports.Film = Film;
//# sourceMappingURL=Film.js.map