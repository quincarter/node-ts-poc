"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Address_1 = require("./Address");
const Payment_1 = require("./Payment");
const Rental_1 = require("./Rental");
let Customer = class Customer {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "customer_id"
    }),
    __metadata("design:type", Number)
], Customer.prototype, "customer_id", void 0);
__decorate([
    typeorm_1.Column("smallint", {
        nullable: false,
        name: "store_id"
    }),
    __metadata("design:type", Number)
], Customer.prototype, "store_id", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 45,
        name: "first_name"
    }),
    __metadata("design:type", String)
], Customer.prototype, "first_name", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 45,
        name: "last_name"
    }),
    __metadata("design:type", String)
], Customer.prototype, "last_name", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: true,
        length: 50,
        name: "email"
    }),
    __metadata("design:type", String)
], Customer.prototype, "email", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Address_1.Address, address => address.customers, { nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'address_id' }),
    __metadata("design:type", Address_1.Address)
], Customer.prototype, "address_", void 0);
__decorate([
    typeorm_1.Column("boolean", {
        nullable: false,
        default: () => "true",
        name: "activebool"
    }),
    __metadata("design:type", Boolean)
], Customer.prototype, "activebool", void 0);
__decorate([
    typeorm_1.Column("date", {
        nullable: false,
        default: () => "('now')::date",
        name: "create_date"
    }),
    __metadata("design:type", String)
], Customer.prototype, "create_date", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: true,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], Customer.prototype, "last_update", void 0);
__decorate([
    typeorm_1.Column("integer", {
        nullable: true,
        name: "active"
    }),
    __metadata("design:type", Number)
], Customer.prototype, "active", void 0);
__decorate([
    typeorm_1.OneToMany(type => Payment_1.Payment, payment => payment.customer_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Customer.prototype, "payments", void 0);
__decorate([
    typeorm_1.OneToMany(type => Rental_1.Rental, rental => rental.customer_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Customer.prototype, "rentals", void 0);
Customer = __decorate([
    typeorm_1.Entity("Customer", { schema: "public" }),
    typeorm_1.Index("idx_fk_address_id", ["address_",]),
    typeorm_1.Index("idx_last_name", ["last_name",]),
    typeorm_1.Index("idx_fk_store_id", ["store_id",])
], Customer);
exports.Customer = Customer;
//# sourceMappingURL=Customer.js.map