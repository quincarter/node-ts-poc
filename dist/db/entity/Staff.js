"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Address_1 = require("./Address");
const Payment_1 = require("./Payment");
const Rental_1 = require("./Rental");
const Store_1 = require("./Store");
let Staff = class Staff {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "staff_id"
    }),
    __metadata("design:type", Number)
], Staff.prototype, "staff_id", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 45,
        name: "first_name"
    }),
    __metadata("design:type", String)
], Staff.prototype, "first_name", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 45,
        name: "last_name"
    }),
    __metadata("design:type", String)
], Staff.prototype, "last_name", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Address_1.Address, address => address.staffs, { nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'address_id' }),
    __metadata("design:type", Address_1.Address)
], Staff.prototype, "address_", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: true,
        length: 50,
        name: "email"
    }),
    __metadata("design:type", String)
], Staff.prototype, "email", void 0);
__decorate([
    typeorm_1.Column("smallint", {
        nullable: false,
        name: "store_id"
    }),
    __metadata("design:type", Number)
], Staff.prototype, "store_id", void 0);
__decorate([
    typeorm_1.Column("boolean", {
        nullable: false,
        default: () => "true",
        name: "active"
    }),
    __metadata("design:type", Boolean)
], Staff.prototype, "active", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 16,
        name: "username"
    }),
    __metadata("design:type", String)
], Staff.prototype, "username", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: true,
        length: 40,
        name: "password"
    }),
    __metadata("design:type", String)
], Staff.prototype, "password", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], Staff.prototype, "last_update", void 0);
__decorate([
    typeorm_1.Column("bytea", {
        nullable: true,
        name: "picture"
    }),
    __metadata("design:type", Buffer)
], Staff.prototype, "picture", void 0);
__decorate([
    typeorm_1.OneToMany(type => Payment_1.Payment, payment => payment.staff_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Staff.prototype, "payments", void 0);
__decorate([
    typeorm_1.OneToMany(type => Rental_1.Rental, rental => rental.staff_),
    __metadata("design:type", Array)
], Staff.prototype, "rentals", void 0);
__decorate([
    typeorm_1.OneToOne(type => Store_1.Store, store => store.manager_staff_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Store_1.Store)
], Staff.prototype, "store", void 0);
Staff = __decorate([
    typeorm_1.Entity("Staff", { schema: "public" })
], Staff);
exports.Staff = Staff;
//# sourceMappingURL=Staff.js.map