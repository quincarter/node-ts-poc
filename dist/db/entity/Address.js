"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const City_1 = require("./City");
const Customer_1 = require("./Customer");
const Staff_1 = require("./Staff");
const Store_1 = require("./Store");
let Address = class Address {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "address_id"
    }),
    __metadata("design:type", Number)
], Address.prototype, "address_id", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 50,
        name: "address"
    }),
    __metadata("design:type", String)
], Address.prototype, "address", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: true,
        length: 50,
        name: "address2"
    }),
    __metadata("design:type", String)
], Address.prototype, "address2", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 20,
        name: "district"
    }),
    __metadata("design:type", String)
], Address.prototype, "district", void 0);
__decorate([
    typeorm_1.ManyToOne(type => City_1.City, city => city.addresss, { nullable: false, }),
    typeorm_1.JoinColumn({ name: 'city_id' }),
    __metadata("design:type", City_1.City)
], Address.prototype, "city_", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: true,
        length: 10,
        name: "postal_code"
    }),
    __metadata("design:type", String)
], Address.prototype, "postal_code", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 20,
        name: "phone"
    }),
    __metadata("design:type", String)
], Address.prototype, "phone", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], Address.prototype, "last_update", void 0);
__decorate([
    typeorm_1.OneToMany(type => Customer_1.Customer, customer => customer.address_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Address.prototype, "customers", void 0);
__decorate([
    typeorm_1.OneToMany(type => Staff_1.Staff, staff => staff.address_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Address.prototype, "staffs", void 0);
__decorate([
    typeorm_1.OneToMany(type => Store_1.Store, store => store.address_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Address.prototype, "stores", void 0);
Address = __decorate([
    typeorm_1.Entity("address", { schema: "public" }),
    typeorm_1.Index("idx_fk_city_id", ["city_",])
], Address);
exports.Address = Address;
//# sourceMappingURL=Address.js.map