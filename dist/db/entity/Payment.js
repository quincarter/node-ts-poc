"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Customer_1 = require("./Customer");
const Staff_1 = require("./Staff");
const Rental_1 = require("./Rental");
let Payment = class Payment {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "payment_id"
    }),
    __metadata("design:type", Number)
], Payment.prototype, "payment_id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Customer_1.Customer, customer => customer.payments, { nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'customer_id' }),
    __metadata("design:type", Customer_1.Customer)
], Payment.prototype, "customer_", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Staff_1.Staff, staff => staff.payments, { nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'staff_id' }),
    __metadata("design:type", Staff_1.Staff)
], Payment.prototype, "staff_", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Rental_1.Rental, rental => rental.payments, { nullable: false, onDelete: 'SET NULL', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'rental_id' }),
    __metadata("design:type", Rental_1.Rental)
], Payment.prototype, "rental_", void 0);
__decorate([
    typeorm_1.Column("numeric", {
        nullable: false,
        precision: 5,
        scale: 2,
        name: "amount"
    }),
    __metadata("design:type", String)
], Payment.prototype, "amount", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        name: "payment_date"
    }),
    __metadata("design:type", Date)
], Payment.prototype, "payment_date", void 0);
Payment = __decorate([
    typeorm_1.Entity("Payment", { schema: "public" }),
    typeorm_1.Index("idx_fk_customer_id", ["customer_",]),
    typeorm_1.Index("idx_fk_rental_id", ["rental_",]),
    typeorm_1.Index("idx_fk_staff_id", ["staff_",])
], Payment);
exports.Payment = Payment;
//# sourceMappingURL=Payment.js.map