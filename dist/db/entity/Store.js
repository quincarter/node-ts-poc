"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Staff_1 = require("./Staff");
const Address_1 = require("./Address");
let Store = class Store {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "store_id"
    }),
    __metadata("design:type", Number)
], Store.prototype, "store_id", void 0);
__decorate([
    typeorm_1.OneToOne(type => Staff_1.Staff, staff => staff.store, { nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'manager_staff_id' }),
    __metadata("design:type", Staff_1.Staff)
], Store.prototype, "manager_staff_", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Address_1.Address, address => address.stores, { nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'address_id' }),
    __metadata("design:type", Address_1.Address)
], Store.prototype, "address_", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], Store.prototype, "last_update", void 0);
Store = __decorate([
    typeorm_1.Entity("Store", { schema: "public" }),
    typeorm_1.Index("idx_unq_manager_staff_id", ["manager_staff_",], { unique: true })
], Store);
exports.Store = Store;
//# sourceMappingURL=Store.js.map