"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Film_1 = require("./Film");
let Language = class Language {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "language_id"
    }),
    __metadata("design:type", Number)
], Language.prototype, "language_id", void 0);
__decorate([
    typeorm_1.Column("character", {
        nullable: false,
        length: 20,
        name: "name"
    }),
    __metadata("design:type", String)
], Language.prototype, "name", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], Language.prototype, "last_update", void 0);
__decorate([
    typeorm_1.OneToMany(type => Film_1.Film, film => film.language_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Language.prototype, "films", void 0);
Language = __decorate([
    typeorm_1.Entity("Language", { schema: "public" })
], Language);
exports.Language = Language;
//# sourceMappingURL=Language.js.map