"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Inventory_1 = require("./Inventory");
const Customer_1 = require("./Customer");
const Staff_1 = require("./Staff");
const Payment_1 = require("./Payment");
let Rental = class Rental {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "rental_id"
    }),
    __metadata("design:type", Number)
], Rental.prototype, "rental_id", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        name: "rental_date"
    }),
    __metadata("design:type", Date)
], Rental.prototype, "rental_date", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Inventory_1.Inventory, inventory => inventory.rentals, { nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'inventory_id' }),
    __metadata("design:type", Inventory_1.Inventory)
], Rental.prototype, "inventory_", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Customer_1.Customer, customer => customer.rentals, { nullable: false, onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'customer_id' }),
    __metadata("design:type", Customer_1.Customer)
], Rental.prototype, "customer_", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: true,
        name: "return_date"
    }),
    __metadata("design:type", Date)
], Rental.prototype, "return_date", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Staff_1.Staff, staff => staff.rentals, { nullable: false, }),
    typeorm_1.JoinColumn({ name: 'staff_id' }),
    __metadata("design:type", Staff_1.Staff)
], Rental.prototype, "staff_", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], Rental.prototype, "last_update", void 0);
__decorate([
    typeorm_1.OneToMany(type => Payment_1.Payment, payment => payment.rental_, { onDelete: 'SET NULL', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Rental.prototype, "payments", void 0);
Rental = __decorate([
    typeorm_1.Entity("Rental", { schema: "public" }),
    typeorm_1.Index("idx_unq_rental_rental_date_inventory_id_customer_id", ["customer_", "inventory_", "rental_date",], { unique: true }),
    typeorm_1.Index("idx_fk_inventory_id", ["inventory_",])
], Rental);
exports.Rental = Rental;
//# sourceMappingURL=Rental.js.map