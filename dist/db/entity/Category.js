"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const FilmCategory_1 = require("./FilmCategory");
let Category = class Category {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "category_id"
    }),
    __metadata("design:type", Number)
], Category.prototype, "category_id", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 25,
        name: "name"
    }),
    __metadata("design:type", String)
], Category.prototype, "name", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], Category.prototype, "last_update", void 0);
__decorate([
    typeorm_1.OneToMany(type => FilmCategory_1.FilmCategory, film_category => film_category.category_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Category.prototype, "film_categorys", void 0);
Category = __decorate([
    typeorm_1.Entity("Category", { schema: "public" })
], Category);
exports.Category = Category;
//# sourceMappingURL=Category.js.map