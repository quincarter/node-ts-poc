"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Country_1 = require("./Country");
const Address_1 = require("./Address");
let City = class City {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "city_id"
    }),
    __metadata("design:type", Number)
], City.prototype, "city_id", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 50,
        name: "city"
    }),
    __metadata("design:type", String)
], City.prototype, "city", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Country_1.Country, country => country.citys, { nullable: false, }),
    typeorm_1.JoinColumn({ name: 'country_id' }),
    __metadata("design:type", Country_1.Country)
], City.prototype, "country_", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], City.prototype, "last_update", void 0);
__decorate([
    typeorm_1.OneToMany(type => Address_1.Address, address => address.city_),
    __metadata("design:type", Array)
], City.prototype, "addresss", void 0);
City = __decorate([
    typeorm_1.Entity("city", { schema: "public" }),
    typeorm_1.Index("idx_fk_country_id", ["country_",])
], City);
exports.City = City;
//# sourceMappingURL=City.js.map