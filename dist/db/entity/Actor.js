"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const FilmActor_1 = require("./FilmActor");
const common_1 = require("@tsed/common");
let Actor = class Actor {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        type: "integer",
        name: "actor_id"
    }),
    common_1.Property(),
    __metadata("design:type", Number)
], Actor.prototype, "actor_id", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 45,
        name: "first_name"
    }),
    common_1.Property(),
    __metadata("design:type", String)
], Actor.prototype, "first_name", void 0);
__decorate([
    typeorm_1.Column("character varying", {
        nullable: false,
        length: 45,
        name: "last_name"
    }),
    common_1.Property(),
    __metadata("design:type", String)
], Actor.prototype, "last_name", void 0);
__decorate([
    typeorm_1.Column("timestamp without time zone", {
        nullable: false,
        default: () => "now()",
        name: "last_update"
    }),
    __metadata("design:type", Date)
], Actor.prototype, "last_update", void 0);
__decorate([
    typeorm_1.OneToMany(type => FilmActor_1.FilmActor, film_actor => film_actor.actor_, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    common_1.Property(),
    __metadata("design:type", Array)
], Actor.prototype, "film_actors", void 0);
Actor = __decorate([
    typeorm_1.Entity("Actor", { schema: "public" }),
    typeorm_1.Index("idx_actor_last_name", ["last_name",])
], Actor);
exports.Actor = Actor;
//# sourceMappingURL=Actor.js.map