"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@tsed/common");
const typeorm_1 = require("@tsed/typeorm");
const Actor_1 = require("../db/entity/Actor");
let ActorService = class ActorService {
    constructor(typeORMService) {
        this.typeORMService = typeORMService;
    }
    $afterRoutesInit() {
        this.connection = this.typeORMService.get("postgres");
    }
    create(actor) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("BODY REQUEST", actor);
            if (!actor.actor_id) {
                const actor = this.connection.getRepository(Actor_1.Actor).findOne({
                    first_name: actor.first_name,
                    last_name: actor.last_name
                });
            }
            this.connection.getRepository(Actor_1.Actor).findOneOrFail({
                first_name: actor.first_name,
                last_name: actor.last_name
            })
                .then((data) => {
                if (data.actor_id) {
                    reject;
                }
            });
            // Then save
            return yield new Promise((resolve, reject) => {
                this.connection.manager.save(actor)
                    .then((data) => {
                    if (data) {
                    }
                });
            });
        });
    }
    save(actor) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.connection.getRepository(Actor_1.Actor).save(actor);
        });
    }
    find() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield new Promise((resolve, reject) => {
                this.connection.manager.find(Actor_1.Actor)
                    .then(data => {
                    if (data.length > 0) {
                        resolve(data);
                    }
                    else {
                        reject("No Records Found");
                    }
                })
                    .catch(() => {
                    console.log("Something went wrong");
                });
            });
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield new Promise((resolve, reject) => {
                this.connection.getRepository(Actor_1.Actor).findOne(id)
                    .then(data => {
                    if (data.actor_id) {
                        resolve(data);
                    }
                    else {
                        reject("No Records Found");
                    }
                }).catch(() => {
                    console.log("Something went wrong");
                });
            });
        });
    }
};
ActorService = __decorate([
    common_1.Service(),
    __metadata("design:paramtypes", [typeorm_1.TypeORMService])
], ActorService);
exports.ActorService = ActorService;
//# sourceMappingURL=ActorService.js.map