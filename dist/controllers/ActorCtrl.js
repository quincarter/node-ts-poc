"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@tsed/common");
const swagger_1 = require("@tsed/swagger");
const ActorService_1 = require("../services/ActorService");
const Actor_1 = require("../db/entity/Actor");
let ActorCtrl = class ActorCtrl {
    constructor(actorService) {
        this.actorService = actorService;
    }
    find() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.actorService.find();
        });
    }
    findActor(id) {
        return this.actorService.findById(id);
    }
    post(actor) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.actorService.create(actor);
        });
    }
    deleteItem(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return { id, name: "calendar" };
        });
    }
};
__decorate([
    common_1.Get("/"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ActorCtrl.prototype, "find", null);
__decorate([
    common_1.Get("/:id"),
    __param(0, common_1.Required()), __param(0, common_1.PathParams("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ActorCtrl.prototype, "findActor", null);
__decorate([
    common_1.Post("/"),
    common_1.Authenticated(),
    __param(0, common_1.Required()), __param(0, common_1.BodyParams("actor")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Actor_1.Actor]),
    __metadata("design:returntype", Promise)
], ActorCtrl.prototype, "post", null);
__decorate([
    common_1.Delete("/"),
    common_1.Authenticated(),
    __param(0, common_1.BodyParams("calendar.id")), __param(0, common_1.Required()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ActorCtrl.prototype, "deleteItem", null);
ActorCtrl = __decorate([
    common_1.Controller("/actors"),
    swagger_1.Docs("api-v1"),
    swagger_1.Title("ActorController"),
    __metadata("design:paramtypes", [ActorService_1.ActorService])
], ActorCtrl);
exports.ActorCtrl = ActorCtrl;
//# sourceMappingURL=ActorCtrl.js.map